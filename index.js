const getCube= 2**3;
const getCubeAnswer = `The cube of 2 is ${getCube}`

console.log(getCubeAnswer);


const address = [
	"258 Washington Ave NW,",
	"California",
	"90011"
	];
const [streetName, stateName, zipCode] = address;
const printAddress = `I live at ${streetName} ${stateName} ${zipCode}`

console.log(printAddress);


const animal = {
	name: "Lolong",
	type: "saltwater",
	animalia: "crocodile",
	weight: "1075kgs",
	length: "20 ft 3 in"
};

const { name, type, animalia, weight, length } = animal;

const printAnimal = `${name} was a ${type} ${animalia}. He weighted at ${weight} with a measurement of ${length}.`

console.log(printAnimal);




const numbers = [1,2,3,4,5];

numbers.forEach((number)=>{
	console.log(number);
})



const reduceNumber = numbers.reduce((total, numbers) => {
  return total + numbers;
}, 0);

console.log(reduceNumber);


class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

const scooby = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(scooby);